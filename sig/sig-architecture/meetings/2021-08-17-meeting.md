# 架构SIG例会 2021-8-17 9:30-11:30(UTC+08:00)Beijing

## 议题(Agenda)

议题1、新增ace_ets_loader和ace_js_loader仓：  
`https://gitee.com/openharmony/ace_ets_loader`  
`https://gitee.com/openharmony/ace_js_loader`  
新增三方开源软件仓weex-loader和parse5：  
`https://gitee.com/openharmony/weex-loader`  
`https://gitee.com/openharmony/parse5`  

## 与会人(Attendees)

任革林 [@im-off-this-week](https://gitee.com/im-off-this-week)  
董金光 [@dongjinguang](https://gitee.com/dongjinguang)  
张勇智 [@karl-z](https://gitee.com/karl-z)  
万承臻 [@wanchengzhen](https://gitee.com/wanchengzhen)  
强波   [@huawei_qiangbo](https://gitee.com/huawei_qiangbo)  

## 会议纪要(Notes)

**议题1、新增ace_ets_loader和ace_js_loader仓：  
`https://gitee.com/openharmony/ace_ets_loader`  
`https://gitee.com/openharmony/ace_js_loader`  
新增三方开源软件仓weex-loader和parse5：  
`https://gitee.com/openharmony/weex-loader`  
`https://gitee.com/openharmony/parse5`**  
汇报人：王纯  
会议结论：  
1、新增4个仓  
`https://gitee.com/openharmony/developtools/ace-js2bundle`  
`https://gitee.com/openharmony/developtools/ace-ets2bundle`  
`https://gitee.com/openharmony/third_party/weex-loader`  
`https://gitee.com/openharmony/third_party/parse5`  
2、打包后的sdk的路径与鸿蒙SDK保持一致，若有微调变动与IDE对齐  
