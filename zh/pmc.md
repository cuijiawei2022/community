## OpenHarmony项目管理委员会（PMC）


   OpenHarmony项目管理委员会（PMC: Project Management Committee）负责OpenHarmony社区管理，职责如下：
1. 负责社区管理工作，包括开源社区版本规划、架构看护、特性代码开发维护、版本及补丁规划等；
2. 发布和处理社区需求，为开源社区提供技术架构指导和技术决策；
3. 组织社区安全工作，及时进行安全漏洞扫描、响应、处理等工作；
4. 处理社区Bug、issue、邮件列表等渠道开发者反馈问题；
5. 负责PMC、Committer成员的[选举和退出](./guidelines_role_growth.md)，制定PMC、Committer协作机制；

###
   OpenHarmony项目管理委员会（PMC: Project Management Committee）设主席一名，主席从PMC成员中选举产生。
   PMC主席职责如下：
1. 负责OpenHarmony社区整体技术治理；
2. 主持OpenHarmony社区PMC、Committer新成员的选举和退出，以及社区协作机制；
3. 召集和主持PMC会议，并检查PMC会议决议的落实情况；
4. 代表PMC或委派PMC成员参与OpenHarmony项目群周边组织会议，以及技术交流活动。



## OpenHarmony PMC成员列表
| 姓名 | 账号   | 角色 | 领域 |
| :----: | :----: | :----: | :----: |
| 任革林 | [@im-off-this-week](https://gitee.com/im-off-this-week) |PMC主席 | 总架构 |
| 董金光 |[@dongjinguang](https://gitee.com/dongjinguang) | PMC成员 | 系统架构 |
| 万承臻 | [@wanchengzhen](https://gitee.com/wanchengzhen) | PMC成员 | 架构SIG |
| 付天福 | [@futianfu](https://gitee.com/futianfu) | PMC成员 |	安全架构 |
| 吴勇辉 | [@davidwulanxi](https://gitee.com/davidwulanxi) | PMC成员 | 版本发布SIG |
| 强波 | [@huawei_qiangbo](https://gitee.com/huawei_qiangbo) | PMC成员 | 应用框架SIG |
| 鲜余强 | [@klooer](https://gitee.com/klooer) | PMC成员 | 编译运行时SIG |
| 余枝强 | [@yuzhiqiang101](https://gitee.com/yuzhiqiang101) | PMC成员 | ArkUI框架SIG |
| 易见 | [@easy-to-see](https://gitee.com/easy-to-see) | PMC成员 | 内核SIG |
| 马耀辉 | [@stesen](https://gitee.com/stesen) | PMC成员 | 基础软件服务SIG |
| 赵文华 | [@shidi_snow](https://gitee.com/shidi_snow) | PMC成员 | 驱动框架SIG |
| 丁勇 | [@ding-yong](https://gitee.com/ding-yong) | PMC成员 | 社区产品规划 |
| 邢文华 | [@xhuazi](https://gitee.com/xhuazi) | PMC成员 | QA-SIG |
| 高涵一 | [@gaohanyi1982](https://gitee.com/gaohanyi1982) | PMC成员 | 测试SIG |
| 王意明 | [@youthdragon](https://gitee.com/youthdragon) | PMC成员 | 基础设施SIG |
| 张小田 | [@handyohos](https://gitee.com/handyohos) | PMC成员 | 基础软件服务SIG |
| 李煜 | [@abbuu](https://gitee.com/abbuu) | PMC成员 | 图形SIG |
| 巴延兴 | [@bayanxing](https://gitee.com/bayanxing) | PMC成员 | 测试SIG |


## PMC会议链接
- 会议时间: 每双周周四 9:30-11:00
- 议题申报: [OpenHarmony PMC Meeting Proposal](https://docs.qingque.cn/s/home/eZQB8yRFQfEFeAxk_6JKZEE0q?identityId=1tbICPd8j3s)
- 会议主题: 通过邮件通知
- 会议通知: 请[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io)邮件列表 dev@openharmony.io 获取会议链接

## 联系方式

| 地址                                 | 简介        | 用途说明                                                         |
| ---------------------------------------|---------- | ------------------------------------------------------------ |
| dev@openharmony.io  <img width=120/>| 开发邮件列表 <img width=100/> | OpenHarmony社区开发讨论邮件列表，任何社区开发相关话题都可以在邮件列表讨论。任何开发者可[订阅](https://lists.openatom.io/postorius/lists/dev.openharmony.io)。<img width=200/>|
| cicd@openharmony.io <img width=120/> | CI邮件列表  <img width=100/>| OpenHarmony CICD构建邮件列表，任何开发者可[订阅](https://lists.openatom.io/postorius/lists/cicd.openharmony.io)。<img width=200/>|
| pmc@openharmony.io  <img width=120/>| PMC邮件列表  <img width=100/>| PMC讨论邮件列表，PMC成员可[订阅](https://lists.openatom.io/postorius/lists/pmc.openharmony.io/)。<img width=200/>|

